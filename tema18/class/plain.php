<?php
class plain{
    private $color;
    private $engine;

     public function __construct($color,$engine){
     $this->color= $color;
     $this->engine=$engine;
    }

    public function setColor($color){
    $this->color=$color;
    }
    public function getColor(){
        return $this->color;
    }
   
    public function setEngine($engine){
     $this->engine=$engine;
    }
    public function getEngine(){
     return $this->engine;
    }
    public function about(){
        echo "Plain color is: ". $this->getColor(). "<br>";
        echo "Type of engine: ". $this->getEngine(). "<br>";
    }
}

?>