<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class DateAndTimeController extends AbstractController
{
    /**
     * @Route("/date/and/time", name="date_and_time")
     */
    public function display()
    {
       date_default_timezone_set('Europe/Bucharest');
       $date = date('m/d/Y h:i:s a', time());
       return $this->render('date_and_time/datetime.html.twig', ['date'=>$date]);
    }
}
